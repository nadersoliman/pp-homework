require('coffee-script');

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['../test/e2e/**/*.coffee'],
  onPrepare: function(){
    global.findBy = protractor.By;
  }
};
