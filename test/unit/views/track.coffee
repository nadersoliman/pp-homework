describe 'TrackViewCtrl', ->
  $scope = null
  $controller = null
  $location = null
  createController = ->
    $location.path '/track'
    $controller 'TrackViewCtrl', {'$scope': $scope}
  
  beforeEach ->
    module 'PPHomework'
    inject ($injector) ->
      $controller = $injector.get '$controller'
      $scope = $injector.get '$rootScope'
      $location = $injector.get '$location'
      
  afterEach ->
    #

  it 'should pass on when user is defined', ->
    #
