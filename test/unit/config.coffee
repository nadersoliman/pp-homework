describe 'ModuleConfig', ->
  original_window_PPHOMEWORKCONFIG = null
  
  beforeEach ->
    original_window_PPHOMEWORKCONFIG = window.PPHOMEWORKCONFIG
    delete window.PPHOMEWORKCONFIG
      
  afterEach ->
    window.PPHOMEWORKCONFIG = original_window_PPHOMEWORKCONFIG 
    
  it 'should raise exception if window.PPHOMEWORKCONFIG is not defined', ->
    exceptionRaised = false
    try
      module 'PPHomework'
      inject ($injector) ->
        #
    catch
      exceptionRaised = true
    expect(exceptionRaised).toBe true
      
  it 'should pass on window.PPHOMEWORKCONFIG defined, with Config.relativeRoot to be /', ->
    window.PPHOMEWORKCONFIG = {}
    relativeRoot = null
    module 'PPHomework'
    inject ($injector) ->
      Config = $injector.get 'Config'
      relativeRoot = Config.getRelativeRoot()
    expect(relativeRoot).toEqual '/'
      
  it 'should pass on window.PPHOMEWORKCONFIG defined, with Config.relativeRoot to be /something', ->
    window.PPHOMEWORKCONFIG =
      relativeRoot = "/something"
    relativeRoot = null
    module 'PPHomework'
    inject ($injector) ->
      Config = $injector.get 'Config'
      relativeRoot = Config.getRelativeRoot()
    expect(relativeRoot).toEqual '/'
      
