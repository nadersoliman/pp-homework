describe 'PPAPI Backend Mocked,', ->
  $httpBackend = null
  ppApi = null
  Config = null
  originalConfig = {}
  testingApiEndPoint = 'http://testing.end.point/api/'
  
  beforeEach ->
    angular.copy window.PPHOMEWORKCONFIG, originalConfig
    window.PPHOMEWORKCONFIG['endPoint'] = testingApiEndPoint
    module 'PPHomework'
    inject ($injector) ->
      $httpBackend = $injector.get '$httpBackend'
      ppApi = $injector.get 'ppApi'
      Config = $injector.get 'Config'
      
  afterEach ->
    angular.copy originalConfig, window.window.PPHOMEWORKCONFIG 
    $httpBackend.verifyNoOutstandingExpectation()
    $httpBackend.verifyNoOutstandingRequest()

  #
  # addAddress
  #
  it 'addAddress should handle success', ->
    address = 
      address: 'some address'
      lng: "123"
      lat: "321"
    $httpBackend.expectPOST("#{Config.getApiEndPoint()}address/", address)
    .respond 201, {message:'OK'}
    
    retMsg = null
    ppApi.addAddress(address)
    .then (msg)->
      retMsg = msg
    , ()->
      #
      
    $httpBackend.flush()
    expect(retMsg).toBe 'OK'
    
  it 'addAddress should handle errors', ->
    address = 
      address: 'some address for error'
      lng: "123"
      lat: "321"
    
    # 409
    $httpBackend.expectPOST("#{Config.getApiEndPoint()}address/", address)
    .respond 409, {message:'ADD_ADDRESS:ADDRESS_ALREADY_EXISTS'}
    
    retMsg = null
    ppApi.addAddress(address)
    .then (msg)->
      a=1
    , (msg)->
      retMsg = msg
      
    $httpBackend.flush()
    expect(retMsg).toBe 'ADD_ADDRESS:ADDRESS_ALREADY_EXISTS'

    # UNKOWN
    $httpBackend.expectPOST("#{Config.getApiEndPoint()}address/", address)
    .respond 500, {message:'ADD_ADDRESS:UNKOWN'}
    
    retMsg = null
    ppApi.addAddress(address)
    .then (msg)->
      a=1
    , (msg)->
      retMsg = msg
      
    $httpBackend.flush()
    expect(retMsg).toBe 'ADD_ADDRESS:UNKOWN'
    
  #
  # deleteAddress
  #
  it 'deleteAddress should handle success', ->
    $httpBackend.expectDELETE("#{Config.getApiEndPoint()}address/123/321")
    .respond 201, {message:'OK'}
    
    retMsg = null
    ppApi.deleteAddress(123, 321)
    .then (msg)->
      retMsg = msg
    , ()->
      #
      
    $httpBackend.flush()
    expect(retMsg).toBe 'OK'
    
  it 'deleteAddress should handle errors', ->
    address = 'address_to_delete_with_error'
    
    # 404
    $httpBackend.expectDELETE("#{Config.getApiEndPoint()}address/123/321")
    .respond 404, {message:'DELETE_ADDRESS:ADDRESS_NOT_FOUND'}
    
    retMsg = null
    ppApi.deleteAddress(123, 321)
    .then (msg)->
      a=1
    , (msg)->
      retMsg = msg
      
    $httpBackend.flush()
    expect(retMsg).toBe 'DELETE_ADDRESS:ADDRESS_NOT_FOUND'

    # UNKOWN
    $httpBackend.expectDELETE("#{Config.getApiEndPoint()}address/123/321")
    .respond 500, {message:'something really bad'}
    
    retMsg = null
    ppApi.deleteAddress(123, 321)
    .then (msg)->
      a=1
    , (msg)->
      retMsg = msg
      
    $httpBackend.flush()
    expect(retMsg).toBe 'DELETE_ADDRESS:UNKOWN'

  #
  # listAddresses
  #
  it 'listAddresses should handle success', ->
    addressList = [{address:'address1'}, {address:'address2'}] 
    $httpBackend.expectGET("#{Config.getApiEndPoint()}address/")
    .respond 200, addressList 
    
    retMsg = null
    ppApi.listAddresses()
    .then (msg)->
      retMsg = msg
    , ()->
      #
      
    $httpBackend.flush()
    #expect(retMsg).toEqual addressList
   
  it 'listAddresses should handle errors', ->

    # UNKOWN
    $httpBackend.expectGET("#{Config.getApiEndPoint()}address/")
    .respond 500, {message:'LIST_ADDRESS:UNKOWN'}
    
    retMsg = null
    ppApi.listAddresses()
    .then (msg)->
      a=1
    , (msg)->
      retMsg = msg
      
    $httpBackend.flush()
    expect(retMsg).toBe 'LIST_ADDRESS:UNKOWN'
