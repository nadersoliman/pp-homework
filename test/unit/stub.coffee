orgConsole = window.console
window.PPHOMEWORKCONFIG = {}
window.console =
  log: (z) ->
    #
  info: (z) ->
    #
  debug: (z) ->
    #
  warn: (z...) ->
    orgConsole.warn z...
  error: (z...) ->
    orgConsole.error z...
  test: (z...) ->
    orgConsole.log 'UNIT TEST : ', z...
