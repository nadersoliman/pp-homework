(function() {
    window._ppHomeworkVersion = "1.0.0";
}).call(this);

(function() {
    angular.module("PPHomework.Views", [ "scripts/views/track.html" ]);
    angular.module("PPHomework.Directives", [ "scripts/directives/footer.html", "scripts/directives/nav.html", "scripts/directives/side_bar.html" ]);
    angular.module("PPHomework", [ "ui.bootstrap", "ngRoute", "ngAnimate", "google-maps", "restangular", "PPHomework.Directives", "PPHomework.Views" ]).config([ "$routeProvider", "$locationProvider", "$provide", "RestangularProvider", function($routeProvider, $locationProvider, $provide, RestangularProvider) {
        var Config, apiEndPoint, conf, relativeRoot, versionString;
        versionString = "PPHomework v" + window._ppHomeworkVersion;
        console.info(versionString);
        if (!window.PPHOMEWORKCONFIG) {
            throw Error("cannot find window.PPHOMEWORKCONFIG, initialization is borken");
        }
        conf = window.PPHOMEWORKCONFIG;
        relativeRoot = conf.relativeRoot ? conf.relativeRoot : "/";
        apiEndPoint = conf.apiEndPoint ? conf.apiEndPoint : "/api/";
        RestangularProvider.setBaseUrl(apiEndPoint);
        Config = function() {
            var PrivateClass, instance;
            function Config() {}
            instance = null;
            Config.get = function() {
                return instance != null ? instance : instance = new PrivateClass();
            };
            PrivateClass = function() {
                function PrivateClass() {
                    this._relativeRoot = relativeRoot;
                    this._apiEndPoint = apiEndPoint;
                }
                PrivateClass.prototype.getRelativeRoot = function() {
                    return this._relativeRoot;
                };
                PrivateClass.prototype.getApiEndPoint = function() {
                    return this._apiEndPoint;
                };
                return PrivateClass;
            }();
            return Config;
        }();
        $provide.constant("Config", Config.get());
        $routeProvider.when(relativeRoot, {
            templateUrl: "scripts/views/track.html",
            controller: "TrackViewCtrl"
        }).otherwise({
            redirectTo: relativeRoot
        });
        return $locationProvider.html5Mode(true);
    } ]);
}).call(this);

(function() {
    angular.module("PPHomework").controller("AppController", [ "$scope", "Config", "ppApi", "$q", function($scope, Config, ppApi, $q) {
        $scope.appName = "PPHomework";
        $scope.themes = [ "Slat", "Amelia" ];
        $scope.addresses = [];
        $scope.alert = null;
        $scope.map = null;
        $scope.changeTheme = function(newTheme) {
            $("#theme").attr("href", "css/index-" + newTheme.toLowerCase() + ".css");
            return $scope.currentTheme = newTheme;
        };
        $scope.setMap = function(map) {
            return $scope.map = map;
        };
        $scope.listAddresses = function() {
            return $scope.addresses;
        };
        $scope.addAddress = function(addressText) {
            var deferred;
            deferred = $q.defer();
            ppApi.codeAddress(addressText).then(function(fullAddress) {
                var foundAddresses;
                foundAddresses = $scope.addresses.filter(function(address) {
                    return address.lng === fullAddress.lng && address.lat === fullAddress.lat;
                });
                if (foundAddresses.length <= 0) {
                    return ppApi.addAddress(fullAddress).then(function() {
                        var newList;
                        newList = [];
                        angular.copy($scope.addresses, newList);
                        newList.push(fullAddress);
                        $scope.addresses = newList;
                        return deferred.resolve("OK");
                    }, function() {
                        return deferred.reject("REST Error");
                    });
                } else {
                    return deferred.reject("Address already marked");
                }
            }, function(ret) {
                return deferred.reject(ret);
            });
            return deferred.promise;
        };
        $scope.removeAddress = function(address) {
            return ppApi.deleteAddress(address.lng, address.lat).then(function() {
                var newList;
                newList = $scope.addresses.filter(function(x) {
                    return x.lat !== address.at && x.lng !== address.lng;
                });
                return $scope.addresses = newList;
            }, function() {
                return $scope.setAlert("Error deleting address");
            });
        };
        $scope.setAlert = function(newAlert) {
            return $scope.alert = newAlert;
        };
        $scope.changeTheme($scope.themes[0]);
        return ppApi.listAddresses().then(function(ret) {
            return $scope.addresses = ret;
        }, function() {
            return $scope.setAlert("Error loading addresses");
        });
    } ]);
}).call(this);

(function() {
    angular.module("PPHomework").service("ppApi", [ "Restangular", "$q", function(Restangular, $q) {
        var UTMAPI;
        UTMAPI = function() {
            var PrivateClass, instance;
            function UTMAPI() {}
            instance = null;
            UTMAPI.get = function() {
                return instance != null ? instance : instance = new PrivateClass();
            };
            PrivateClass = function() {
                function PrivateClass() {}
                PrivateClass.prototype.addAddress = function(addressToAdd) {
                    var deferred, errorMap;
                    errorMap = {
                        409: "ADD_ADDRESS:ADDRESS_ALREADY_EXISTS"
                    };
                    deferred = $q.defer();
                    Restangular.all("address/").post(addressToAdd, null).then(function(ret) {
                        return deferred.resolve("OK");
                    }, function(ret) {
                        return deferred.reject(_.has(errorMap, ret.status) ? errorMap[ret.status] : "ADD_ADDRESS:UNKOWN");
                    });
                    return deferred.promise;
                };
                PrivateClass.prototype.deleteAddress = function(lng, lat) {
                    var deferred, errorMap;
                    errorMap = {
                        404: "DELETE_ADDRESS:ADDRESS_NOT_FOUND"
                    };
                    deferred = $q.defer();
                    Restangular.all("address/" + lng + "/" + lat).remove(null).then(function(ret) {
                        return deferred.resolve("OK");
                    }, function(ret) {
                        return deferred.reject(_.has(errorMap, ret.status) ? errorMap[ret.status] : "DELETE_ADDRESS:UNKOWN");
                    });
                    return deferred.promise;
                };
                PrivateClass.prototype.listAddresses = function() {
                    var deferred;
                    deferred = $q.defer();
                    Restangular.one("address/").get(null).then(function(ret) {
                        return deferred.resolve(ret);
                    }, function(ret) {
                        return deferred.reject("LIST_ADDRESS:UNKOWN");
                    });
                    return deferred.promise;
                };
                PrivateClass.prototype.codeAddress = function(addressText) {
                    var deferred, geocoder;
                    deferred = $q.defer();
                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        address: addressText
                    }, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            return deferred.resolve({
                                address: results[0].formatted_address,
                                lat: results[0].geometry.location.lat().toString(),
                                lng: results[0].geometry.location.lng().toString()
                            });
                        } else {
                            return deferred.reject("Failed to geo-code");
                        }
                    });
                    return deferred.promise;
                };
                return PrivateClass;
            }();
            return UTMAPI;
        }();
        return UTMAPI.get();
    } ]);
}).call(this);

angular.module("scripts/views/track.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("scripts/views/track.html", "<div google-map center='center' zoom='zoom' draggable=\"true\" events=\"events\">\n" + '	<markers models="markers" coords="\'coords\'" options="\'options\'"></markers>\n' + "</div>\n" + "");
} ]);

(function() {
    angular.module("PPHomework").controller("TrackViewCtrl", [ "$scope", "$timeout", "$location", function($scope, $timeout, $location) {
        $scope.markers = [];
        $scope.$watch("addresses", function() {
            var address, newMarkers, _i, _len, _ref;
            newMarkers = [];
            _ref = $scope.addresses;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                address = _ref[_i];
                newMarkers.push({
                    coords: {
                        latitude: parseFloat(address.lat),
                        longitude: parseFloat(address.lng)
                    },
                    options: {
                        title: address.address
                    }
                });
            }
            console.log("new markers, ", newMarkers);
            return $scope.markers = newMarkers;
        });
        return angular.extend($scope, {
            center: {
                latitude: 52.03946090664634,
                longitude: 10.340087109374947
            },
            zoom: 7,
            events: {
                zoom_changed: function(map, eventName, args) {},
                center_changed: function(map, eventName, args) {},
                click: function(map, eventName, args) {},
                tilesloaded: function(map) {
                    return $scope.$apply(function() {
                        return $scope.setMap(map);
                    });
                }
            }
        });
    } ]);
}).call(this);

angular.module("scripts/directives/footer.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("scripts/directives/footer.html", "<footer>\n" + '	<div class="row">\n' + '		<div class="col-md-12">\n' + "			<p>\n" + '				Sample Application {{appNameAr}} - Created by <a href="mailto:nadersoliman@gmail.com">nadersoliman@gmail.com</a>\n' + "			</p>\n" + "		</div>\n" + "	</div>\n" + "</footer>\n" + "\n" + "");
} ]);

(function() {
    angular.module("PPHomework").directive("footer", [ function() {
        return {
            restrict: "A",
            templateUrl: "scripts/directives/footer.html",
            replace: true,
            scope: false,
            controller: [ "$scope", function($scope) {} ],
            link: function($scope, elem, attr) {}
        };
    } ]);
}).call(this);

angular.module("scripts/directives/nav.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("scripts/directives/nav.html", '<div class="navbar navbar-default navbar-fixed-top" role="navigation">\n' + '	<div class="container">\n' + '		<div class="navbar-header">\n' + '			<a ng-href="{{Config.getRelativeRoot()}}" class="navbar-brand">{{appName}}</a>\n' + '			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">\n' + '				<span class="icon-bar"></span>\n' + '				<span class="icon-bar"></span>\n' + '				<span class="icon-bar"></span>\n' + "			</button>\n" + "		</div>\n" + '		<div class="navbar-collapse collapse" id="navbar-main">\n' + '			<ul class="nav navbar-nav">\n' + "			</ul>\n" + "\n" + '			<ul class="nav navbar-nav navbar-right">\n' + '				<li class="dropdown">\n' + '					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Theme {{currentTheme}}<span class="caret"></span></a>\n' + '					<ul class="dropdown-menu" aria-labelledby="download">\n' + '						<li ng-repeat="theme in themes">\n' + '							<a href="" ng-click="changeTheme(theme)" ng-hide="theme == currentTheme">{{theme}}</a>\n' + "						</li>\n" + "					</ul>\n" + "				</li>\n" + "			</ul>\n" + "\n" + "		</div>\n" + "	</div>\n" + "</div>\n" + "");
} ]);

(function() {
    angular.module("PPHomework").directive("nav", [ function() {
        return {
            restrict: "A",
            templateUrl: "scripts/directives/nav.html",
            replace: true,
            scope: false,
            controller: [ "$scope", function($scope) {} ],
            link: function($scope, elem, attr) {}
        };
    } ]);
}).call(this);

angular.module("scripts/directives/side_bar.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("scripts/directives/side_bar.html", '<ul class="list-group">\n' + '	<li class="list-group-item">\n' + '		<form class="form-inline" role="form" ng-submit="insertAddress()">\n' + '			<div class="form-group">\n' + '				<input type="text" class="form-control" placeholder="Add New Address" ng-model="address" required name="address">\n' + "			</div>\n" + '			<button type="submit" class="btn btn-primary" ng-disabled="!address || !map">\n' + "				Add\n" + "			</button>\n" + "		</form>\n" + "	</li>\n" + '	<li class="list-group-item repeat-animation" ng-animate="\'animate\'" ng-repeat="address in listAddresses()" >\n' + '		{{address.address}} <span class="glyphicon glyphicon-remove-circle" style="cursor: pointer" ng-click="removeAddress(address)"></span>\n' + "	</li>\n" + "</ul>\n" + '<alert ng-show="alert" class="alert alert-danger">{{alert}}</alert>\n' + "");
} ]);

(function() {
    angular.module("PPHomework").directive("sideBar", [ function() {
        return {
            restrict: "A",
            templateUrl: "scripts/directives/side_bar.html",
            replace: false,
            scope: true,
            controller: [ "$scope", function($scope) {
                $scope.address = null;
                return $scope.insertAddress = function() {
                    return $scope.addAddress($scope.address).then(function() {
                        return $scope.address = null;
                    }, function(ret) {
                        return $scope.setAlert(ret);
                    });
                };
            } ],
            link: function($scope, elem, attr) {}
        };
    } ]);
}).call(this);