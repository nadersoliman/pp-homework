angular.module('PPHomework.Views', ['scripts/views/track.html'])

angular.module('PPHomework.Directives', ['scripts/directives/footer.html', 'scripts/directives/nav.html', 
                                          'scripts/directives/side_bar.html'])

angular.module('PPHomework', ['ui.bootstrap', 'ngRoute', 'ngAnimate', 'google-maps', 'restangular', 
                            'PPHomework.Directives', 'PPHomework.Views'])
.config(['$routeProvider', '$locationProvider', '$provide', 'RestangularProvider'
  ($routeProvider, $locationProvider, $provide, RestangularProvider)->
    versionString = "PPHomework v#{window._ppHomeworkVersion}"
    console.info versionString
    
    if not window.PPHOMEWORKCONFIG
      throw Error('cannot find window.PPHOMEWORKCONFIG, initialization is borken')

    conf = window.PPHOMEWORKCONFIG
    relativeRoot = if conf.relativeRoot then conf.relativeRoot else '/'
    apiEndPoint = if conf.apiEndPoint then conf.apiEndPoint else '/api/'
    
    RestangularProvider.setBaseUrl apiEndPoint
    
    class Config
      instance = null
      @get: ()->
        instance ?= new PrivateClass()
      class PrivateClass
        constructor: ()->
          @_relativeRoot = relativeRoot
          @_apiEndPoint = apiEndPoint
        getRelativeRoot: ->
          @_relativeRoot
        getApiEndPoint: ->
          @_apiEndPoint
    
    $provide.constant 'Config', Config.get()
    
    $routeProvider
      .when relativeRoot,
        templateUrl: 'scripts/views/track.html'
        controller: 'TrackViewCtrl'
      .otherwise
        redirectTo: relativeRoot
    $locationProvider.html5Mode(true);
])
