angular.module('PPHomework')
.controller 'AppController', ['$scope', 'Config', 'ppApi', '$q',
  ($scope, Config, ppApi, $q)->         
    $scope.appName = 'PPHomework'
    $scope.themes = ['Slat', 'Amelia']
    $scope.addresses = []
    $scope.alert = null
    $scope.map = null
    
    $scope.changeTheme = (newTheme)->
      #console.log 'changing theme to', newTheme
      $('#theme').attr 'href', "css/index-#{newTheme.toLowerCase()}.css"
      $scope.currentTheme = newTheme
      
    $scope.setMap = (map)->
      $scope.map = map
      
    $scope.listAddresses = ()->
      $scope.addresses
      
    $scope.addAddress = (addressText)->
      deferred = $q.defer()
      
      ppApi.codeAddress(addressText)
      .then (fullAddress)->
        #console.log 'new address is', fullAddress
        foundAddresses = $scope.addresses.filter (address)->
          address.lng is fullAddress.lng and address.lat is fullAddress.lat
        
        if foundAddresses.length <= 0
          ppApi.addAddress(fullAddress)
          .then ->
            #console.log 'address added'
            newList = []
            angular.copy $scope.addresses, newList 
            newList.push fullAddress
            $scope.addresses = newList
            deferred.resolve 'OK'
          , ->
            #console.log 'address not added'
            deferred.reject 'REST Error'
        else
          deferred.reject 'Address already marked'
      , (ret) ->
        deferred.reject ret
      deferred.promise
      
    $scope.removeAddress = (address)->
      #console.log 'removing address', address
      ppApi.deleteAddress(address.lng, address.lat)
      . then ->
        newList = $scope.addresses.filter (x)->
          x.lat isnt address.at and x.lng isnt address.lng
        $scope.addresses = newList
      , ->
        $scope.setAlert 'Error deleting address' 
      
    $scope.setAlert = (newAlert)->
      $scope.alert = newAlert

    $scope.changeTheme $scope.themes[0]
    
    ppApi.listAddresses()
    . then (ret) ->
      $scope.addresses = ret
    , ->
      $scope.setAlert 'Error loading addresses' 
]

