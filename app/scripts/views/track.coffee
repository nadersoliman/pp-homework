angular.module('PPHomework')
.controller 'TrackViewCtrl', ['$scope', '$timeout', '$location'
  ($scope, $timeout, $location)->
    
    $scope.markers = []
    $scope.$watch 'addresses', ()->
      newMarkers = []
      for address in $scope.addresses
        newMarkers.push
          coords:
            latitude: parseFloat(address.lat)
            longitude: parseFloat(address.lng)
          options:
            title: address.address
      console.log 'new markers, ', newMarkers
      $scope.markers = newMarkers
      
    angular.extend $scope,
      center:
        latitude: 52.03946090664634
        longitude: 10.340087109374947
      zoom: 7
      events:
        zoom_changed: (map, eventName, args)->
          #console.log 'zoom_changed with arguments:', arguments
        center_changed: (map, eventName, args)->
          #console.log 'center_changed with arguments:', arguments
        click: (map, eventName, args)->
          #console.log 'click with arguments:', arguments
          #console.log "click @ lat:#{args[0].latLng.lat()}, lng:#{args[0].latLng.lng()}"
          #$scope.$apply ->
          #  $scope.markers.push {latitude:args[0].latLng.lat(), longitude:args[0].latLng.lng()}
        tilesloaded: (map)->
          $scope.$apply ->
            $scope.setMap map
]
