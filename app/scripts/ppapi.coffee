angular.module('PPHomework')
.service 'ppApi', ['Restangular', '$q',
  (Restangular, $q)->
    class UTMAPI
      instance = null
      @get: ()->
        instance ?= new PrivateClass()
      class PrivateClass
        constructor: ()->
          
        addAddress: (addressToAdd)->
          errorMap =
            409: 'ADD_ADDRESS:ADDRESS_ALREADY_EXISTS'
          
          deferred = $q.defer()
          
          Restangular
          .all('address/')
          .post(addressToAdd, null)
          .then (ret)->
            deferred.resolve('OK')
          , (ret)->
              deferred.reject if _.has(errorMap, ret.status) then errorMap[ret.status] else 'ADD_ADDRESS:UNKOWN'
          deferred.promise
          
        deleteAddress: (lng, lat)->
          errorMap =
            404: 'DELETE_ADDRESS:ADDRESS_NOT_FOUND'

          deferred = $q.defer()

          Restangular
          .all("address/#{lng}/#{lat}")
          .remove(null)
          .then (ret)->
            deferred.resolve('OK')
          , (ret)->
              deferred.reject if _.has(errorMap, ret.status) then errorMap[ret.status] else 'DELETE_ADDRESS:UNKOWN'
          deferred.promise
          
        listAddresses: ()->

          deferred = $q.defer()

          Restangular
          .one('address/')
          .get(null)
          .then (ret)->
            deferred.resolve ret
          , (ret)->
              deferred.reject 'LIST_ADDRESS:UNKOWN'
          deferred.promise
          
        codeAddress: (addressText)->
          deferred = $q.defer()
          geocoder = new google.maps.Geocoder()

          geocoder.geocode {'address': addressText}, (results, status) ->
            if status is google.maps.GeocoderStatus.OK
              deferred.resolve 
                address: results[0].formatted_address
                lat: results[0].geometry.location.lat().toString()
                lng: results[0].geometry.location.lng().toString()
            else
              deferred.reject 'Failed to geo-code'
          deferred.promise
          
    UTMAPI.get()
]
