angular.module('PPHomework')
  .directive 'nav', [()->
      restrict: 'A'
      templateUrl: 'scripts/directives/nav.html'
      replace: true
      scope: false
      controller: ['$scope', ($scope) ->
      ]
      link: ($scope, elem, attr) ->
  ] 