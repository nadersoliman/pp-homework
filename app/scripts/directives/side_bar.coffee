angular.module('PPHomework')
  .directive 'sideBar', [()->
      restrict: 'A'
      templateUrl: 'scripts/directives/side_bar.html'
      replace: false
      scope: true
      controller: ['$scope', ($scope) ->
        $scope.address = null
        $scope.insertAddress = ()->
          $scope.addAddress($scope.address)
          .then ->
            $scope.address = null
          , (ret)->
            $scope.setAlert ret 
      ]
      link: ($scope, elem, attr) ->
  ] 