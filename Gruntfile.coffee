LIVERELOAD_PORT = 35729
lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT })
modRewrite = require('connect-modrewrite')

mountFolder = (connect, dir) ->
  return connect.static(require('path').resolve(dir))

# Globbing
# for performance reasons we're only matching one level down:
# 'test/spec/{,*/}*.js'
# use this if you want to recursively match all subfolders:
# 'test/spec/**/*.js'

module.exports = (grunt)->
  #load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)
  pathsConfig = 
    app: 'app'
    dist: 'dist'
    
  packageJson = require './package.json'
  pathsConfig['version'] = packageJson.version
  
  gruntConfig = 
    pathsConfig: pathsConfig 
    watch:
      coffee:
        files: ['<%= pathsConfig.app %>/scripts/**/*.coffee']
        tasks: ['coffee']
      html2js:
        files: ['<%= pathsConfig.app %>/scripts/**/*.html']
        tasks: ['html2js']
      less:
        files: ['<%= pathsConfig.app %>/css/**/*.less']
        tasks: ['less:dist']
      protractor:
        files: ['test/e2e/**/*.coffee']
        tasks: ['protractor:e2e']
      livereload:
        options:
          livereload: LIVERELOAD_PORT
        files: [
          '<%= pathsConfig.app %>/*.html'
          '{.tmp,<%= pathsConfig.app %>}/css/*.css'
          '<%= pathsConfig.app %>/img/*.{png,jpg,jpeg,gif,webp,svg}'
          '.tmp/scripts/**/*.js'
        ]
    connect:
      options:
        port: 9001,
        # Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost'
      livereload:
        options:
          middleware: (connect) ->
            return [
              # http://ericduran.io/2013/05/31/angular-html5Mode-with-yeoman/
              modRewrite(['!\\.html|\\.js|\\.css|\\.png|\\.woff|\\.ttf|\\.eot$ /index.html [L]'])              
              lrSnippet
              mountFolder(connect, '.tmp')
              mountFolder(connect, pathsConfig.app)
              mountFolder(connect, 'bower_components')
            ]
      dist:
        options:
          middleware: (connect) ->
            return [
              mountFolder(connect, pathsConfig.dist)
            ]
    open:
      server:
        url: 'http://localhost:<%= connect.options.port %>'
    clean:
      dist:
        files: [
          dot: true,
          src: ['.tmp', '<%= pathsConfig.dist %>/*', '!<%= pathsConfig.dist %>/.git*']
        ]
      server: '.tmp'
      coverage:
        files: [
          dot: true,
          src: ['coverage/*']
        ]
      deploy:
        options: force: true
        files: [
          dot: true
          src: '../pp-server/static/**/*'
        ]
    coffee:
      dist:
        files: [
          expand: true,
          cwd: '<%= pathsConfig.app %>'
          src: ['scripts/**/*.coffee']
          dest: '.tmp'
          ext: '.js'
        ]
    useminPrepare:
      html: ['<%= pathsConfig.app %>/index.html']
    usemin:
      html: ['<%= pathsConfig.dist %>/*.html']
    htmlmin:
      dist:
        #options:
          #removeCommentsFromCDATA: true
          #https://github.com/yeoman/grunt-usemin/issues/44
          #collapseWhitespace: true
          #collapseBooleanAttributes: true
          #removeAttributeQuotes: true
          #removeRedundantAttributes: true
          #useShortDoctype: true
          #removeEmptyAttributes: true
          #removeOptionalTags: true
        files: [
          expand: true
          cwd: '<%= pathsConfig.app %>'
          src: ['*.html']
          dest: '<%= pathsConfig.dist %>'
        ]
    imagemin:
      dist:
        files: [
          expand: true
          cwd: '<%= pathsConfig.app %>/img'
          src: '*.{png,jpg,jpeg}'
          dest: '<%= pathsConfig.dist %>/img'
        ]
    cssmin:
      dis:
        files: [
          expand: true
          cwd: '.tmp/css'
          src: ['*.css']
          dest: 'dist/css'
        ]
    concurrent:
      options:
        limit: 5
      server: [
        'coffee' 
        'html2js'
        'less'
      ]
      coverage: [
        'coffee'
        'html2js'
        'less'
      ]
      dist: [
        'coffee'
        'html2js'
        'less'
        'imagemin'
        'htmlmin'
      ]
    uglify:
      options:
        mangle: false
        compress: false # don't enable, drops unused functions
        beautify: true
    less:
      dist:
        options:
          paths: ['<%= pathsConfig.app %>/css', 'bower_components/bootstrap/less']
        files:
          '.tmp/css/index-amelia.css':'<%= pathsConfig.app %>/css/index-amelia.less'
          '.tmp/css/index-slat.css':'<%= pathsConfig.app %>/css/index-slat.less'
    html2js:
      dist:
        options:
          module: null # no bundle module for all the html2js templates
          base: '<%= pathsConfig.app %>'
        files: [
          expand: true
          cwd: '<%= pathsConfig.app %>'
          src: ['scripts/**/*.html']
          dest: '.tmp'
          ext: '.html.js'
        ]
            
    # Put files not handled in other tasks here
    copy:
      dist:
        files: [
          expand: true
          dot: true
          cwd: '<%= pathsConfig.app %>'
          dest: '<%= pathsConfig.dist %>'
          src: ['img/*']
        ,
          expand: true
          dot: true
          cwd: 'bower_components/bootstrap/'
          dest: '<%= pathsConfig.dist %>'
          src: ['fonts/*']
        ]
      server:
        files: [
          expand: true
          dot: true
          cwd: '<%= pathsConfig.app %>'
          dest: '.tmp'
          src: ['img/*']
        ,
          expand: true
          dot: true
          cwd: 'bower_components/bootstrap/'
          dest: '.tmp'
          src: ['fonts/*']
        ]
      deploy:
        files: [
          expand: true
          dot: true
          cwd: 'dist'
          dest: '../pp-server/static'
          src: ['**/*']
        ]
      coverage:
        files: [
          expand: true
          dot: true
          cwd: '<%= pathsConfig.app %>/scripts'
          dest: '.tmp/scripts'
          src: ['layout.yaml']
        ]
    karma:
      unit:
        configFile: './karma/unit.coffee'
      coverage:
        configFile: './karma/coverage.coffee'
      build:
        configFile: './karma/build.coffee'
    protractor:
      options:
        configFile: 'node_modules/protractor/referenceConf.js' # Default config file
        noColor: false      # If true, protractor will not use colors in its output.
        args: {} # Arguments passed to the command
      e2e:
        options:
          configFile: 'protractor/e2e.js' # Target-specific config file
          keepAlive: true    # If false, the grunt process stops when the test fails.
          args: {} # Target-specific arguments
      build:
        options:
          configFile: 'protractor/e2e.js' # Target-specific config file
          keepAlive: false
          args: {} # Target-specific arguments
  
  grunt.initConfig gruntConfig
  
  grunt.registerTask 'version', ->
    require('fs').writeFileSync './app/scripts/version.coffee', "window._ppHomeworkVersion = '#{gruntConfig.pathsConfig.version}'"
  
  grunt.registerTask 'server', [
    'version'
    'clean:server'
    'concurrent:server'
    'copy:server'
    'connect:livereload'
    'open:server'
    'watch'
    ]

  grunt.registerTask 'e2e', [
    'clean:server'
    'concurrent:server'
    'connect:livereload'
    'protractor:e2e'
    'watch'
  ]
    
  grunt.registerTask 'unit', [
    'karma:unit'
  ]
  
  grunt.registerTask 'tests', [
    'clean:server'
    'karma:build'
    'clean:server'
    'concurrent:server'
    'connect:livereload'
    'protractor:build'
  ]

  grunt.registerTask 'coverage', [
    'clean:server'
    'clean:coverage'
    'concurrent:coverage'
    'copy:coverage'
    'karma:coverage'
    ]

  # we should explicitly call uglify, cssmin after useminPrepare as per http://stackoverflow.com/a/19471329/161718
  grunt.registerTask 'build', [
    'version'
    'clean:server'
    'karma:build'
    'clean:server'
    'clean:dist'
    'useminPrepare'
    'concurrent:dist'
    'concat'
    'copy:dist'
    'cssmin'
    'uglify'
    'usemin'
    'connect:dist'
    #'protractor:build'
  ]
  
  # builds and deploys to ../pp-server
  grunt.registerTask 'deploy', [
    'build'
    'clean:deploy'
    'copy:deploy'
  ]    

