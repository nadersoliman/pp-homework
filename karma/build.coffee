module.exports = (karma)->
  baseKarmaFn = require './base.coffee'
  baseKarmaFn karma
  
  karma.files.push '../test/unit/**/*.coffee'
  karma.singleRun = true
  
  karma.set
    browsers: ['Chrome']
