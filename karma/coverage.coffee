module.exports = (karma) ->
  karma.set 
    basePath: '../.tmp/'
    frameworks: ['jasmine']
    files: [
      {pattern: 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js', watched:false, included:true, served:true},
      {pattern: 'http://code.jquery.com/jquery-2.1.0.min.js', watched:false, included:true, served:true},
      {pattern: 'http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js', watched:false, included:true, served:true},
      {pattern: 'http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-route.min.js', watched:false, included:true, served:true},
      {pattern: 'http://code.angularjs.org/1.2.16/angular-animate.min.js', watched:false, included:true, served:true},
      {pattern: 'http://cdnjs.cloudflare.com/ajax/libs/restangular/1.3.1/restangular.min.js', watched:false, included:true, served:true},
      {pattern: 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCQ_GYDu12LDNmJudB4SL8usJW5-VTFAfY&sensor=false', watched:false, included:true, served:true},
      {pattern: '../bower_components/underscore/underscore.js', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-ui-bootstrap/dist/ui-bootstrap-tpls-0.10.0.js', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-google-maps/dist/angular-google-maps.js', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-mocks/angular-mocks.js', watched:false, included:true, served:true},
      #{pattern: '../test/unit/stub.coffee', watched:false, included:true, served:true},
      'scripts/version.js',
      'scripts/config.js',
      'scripts/**/*.js',
      '../test/unit/**/*.coffee'
    ]
    
    # list of files to exclude
    exclude: [
      'scripts/*.html.js'
    ]

    # the following details listing to avoid including .html.js in the coverage report    
    preprocessors:
      '../test/unit/**/*.coffee': ['coffee']
      #'scripts/**/*.js': ['coverage']
      
      'scripts/version.js': ['coverage']
      'scripts/config.js': ['coverage']
      'scripts/app.js': ['coverage']
      'scripts/views/track.js': ['coverage']
        
    browsers: ['Chrome']
          
    # test results reporter to use
    # possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress', 'coverage']
    
    # enable / disable colors in the output (reporters and logs)
    colors: true
    
    # level of logging
    # possible values: karma.LOG_DISABLE || karma.LOG_ERROR || karma.LOG_WARN || karma.LOG_INFO || karma.LOG_DEBUG
    logLevel: karma.LOG_INFO

    # enable / disable watching file and executing tests whenever any file changes
    autoWatch: false

    # If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000

    # Continuous Integration mode
    # if true, it capture browsers, run tests and exit
    singleRun: true
    
    coffeePreprocessor:
      # options passed to the coffee compiler
      options:
        bare: false
        sourceMap: false
        
      # transforming the filenames
      transformPath: (path)->
        path.replace(/\.js$/, '.coffee')
        
    coverageReporter:
      type : 'html'
      dir : '../coverage/unit'
      #file : 'coverage.unit.text'
