module.exports = (karma)->
  karma.set
    # base path, that will be used to resolve files and exclude 
    basePath: '../app/'

    # frameworks to use
    frameworks: ['jasmine', 'browserify']

    # list of files / patterns to load in the browser
    files: [
      {pattern: 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js', watched:false, included:true, served:true},
      {pattern: 'http://code.jquery.com/jquery-2.1.0.min.js', watched:false, included:true, served:true},
      {pattern: 'http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js', watched:false, included:true, served:true},
      {pattern: 'http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-route.min.js', watched:false, included:true, served:true},
      {pattern: 'http://code.angularjs.org/1.2.16/angular-animate.min.js', watched:false, included:true, served:true},
      {pattern: 'http://cdnjs.cloudflare.com/ajax/libs/restangular/1.3.1/restangular.min.js', watched:false, included:true, served:true},
      {pattern: 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCQ_GYDu12LDNmJudB4SL8usJW5-VTFAfY&sensor=false', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-ui-bootstrap/dist/ui-bootstrap-tpls-0.10.0.js', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-google-maps/dist/angular-google-maps.js', watched:false, included:true, served:true},
      {pattern: '../bower_components/angular-mocks/angular-mocks.js', watched:false, included:true, served:true},
      {pattern: '../test/unit/stub.coffee', watched:false, included:true, served:true},
      {pattern: 'scripts/**/*.html', watched:true, included:true, served:true},
      'scripts/version.coffee',
      'scripts/config.coffee',
      'scripts/app.coffee',
      'scripts/ppapi.coffee',
      'scripts/**/*.coffee',
    ]
    
    # list of files to exclude
    exclude: [
    ]

    preprocessors:
      '../**/*.coffee': ['coffee']
      '**/*.html': ['ng-html2js']

    # test results reporter to use
    # possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress']

    # enable / disable colors in the output (reporters and logs)
    colors: true

    # level of logging
    # possible values: karma.LOG_DISABLE || karma.LOG_ERROR || karma.LOG_WARN || karma.LOG_INFO || karma.LOG_DEBUG
    logLevel: karma.LOG_INFO

    # enable / disable watching file and executing tests whenever any file changes
    autoWatch: true

    # If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000

    # Continuous Integration mode
    # if true, it capture browsers, run tests and exit
    singleRun: false
    
    coffeePreprocessor:
      # options passed to the coffee compiler
      options:
        bare: false
        sourceMap: false
      # transforming the filenames
      #transformPath: (path)->
      #  return path.replace(/\.js$/, '.coffee')
    browserify: 
      extensions: ['.coffee']
      #ignore: [path.join __dirname, 'components/angular-unstable/angular.js']
      transform: ['coffeeify']
      watch: true   # Watches dependencies only (Karma watches the tests)
      debug: false   # Adds source maps to bundle
